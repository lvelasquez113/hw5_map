﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Homework5_map.Pages
{
    
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public ObservableCollection<string> list2 { get; set; }
        public Page3()
        {
           InitializeComponent();
            location();
            pins();
        }
        void location()
        {
            //makes a list for the picker
            list2 = new ObservableCollection<string>();

            //adds the string of the title for the specific cell
            list2.Add("Favorite Amusement park");
            list2.Add("Grandma's House");
            list2.Add("Favorite Team");
            list2.Add("Last Hotel stayed in");

            //adds the items to the picker
            picker.ItemsSource = list2;

        }
        void pins()
        {
            //sets the pin positon, label, and address
            var pin = new Pin()
            {
                Position = new Position(34.420263, -118.591200),// makes the position for the pin at the provided coordinates
                Label = "Six Flags Magic Moutain", //labels the pin 
                Address = "26101 Magic Mountain Pkwy, Valencia, CA 91355" //sets the address of the pinned location
            };
            var pin1 = new Pin()
            {
                Position = new Position(34.276849, -118.424557), // makes the position for the pin at the provided coordinates
                Label = "Grandma's House", //labels the pin 
                Address = "13400 Eustace St, Pacoima, CA 91331" //sets the address of the pinned location
            };
            var pin2 = new Pin()
            {
                Position = new Position(32.708045, -117.156840), // makes the position for the pin at the provided coordinates
                Label = "Petco Park", //labels the pin 
                Address = "100 Park Blvd, San Diego, CA 92101" //sets the address of the pinned location
            };
            var pin3 = new Pin()
            {
                Position = new Position(36.099168, -115.175451), // makes the position for the pin at the provided coordinates
                Label = "Excaliber hotel & Casino", //labels the pin 
                Address = "3850 S Las Vegas Blvd, Las Vegas, NV 89109" //sets the address of the pinned location
            };
            //adds the pin to the location specified by the coordinates on the map
            map3.Pins.Add(pin);
            map3.Pins.Add(pin1);
            map3.Pins.Add(pin2);
            map3.Pins.Add(pin3);
        }
        private void picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var pick = sender as Picker;
            int choice = pick.SelectedIndex;
            if (choice != -1)
            {
                if (choice == 0)
                {
                    map3.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(34.420263, -118.591200), Distance.FromMiles(10)));
                }
                if (choice == 1)
                {
                    map3.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(34.276849, -118.424557), Distance.FromMiles(10)));
                }
                if (choice == 2)
                {
                    map3.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(32.708045, -117.156840), Distance.FromMiles(10)));
                }
                if (choice == 3)
                {
                    map3.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(36.099168, -115.175451), Distance.FromMiles(10)));
                }
            }
        }
    }
}