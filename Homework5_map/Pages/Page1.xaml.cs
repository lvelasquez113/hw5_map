﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;

namespace Homework5_map.Pages
{
  
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public ObservableCollection<string> list { get; set; }
        public Page1()
        {
            InitializeComponent();
            location();
            pins();
        }
        //sets the starting point to the school
        // map1.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.127711, -117.159235), Distance.FromMiles(10)));

        //this will populate the picker menu
        void location()
        {
            //makes a list for the picker
            list = new ObservableCollection<string>();

            //adds the string of the title for the specific cell
            list.Add("First House");
            list.Add("Elementary");
            list.Add("Place of Birth");
            list.Add("Vacation");

            //adds the items to the picker
            picker.ItemsSource = list;

        }
            //this gets the pins declared and set on the map
        void pins()
        {
            //sets the pin positon, label, and address
            var pin = new Pin()
            {
                Position = new Position(34.244272, -118.442173),// makes the position for the pin at the provided coordinates
                Label = "Old Parents house", //labels the pin 
                Address = "14201 Gruen St Arleta, CA 91331" //sets the address of the pinned location
            };
            var pin1 = new Pin()
            {
                Position = new Position(33.143447, -117.203117), // makes the position for the pin at the provided coordinates
                Label = "Alvin Dunn/La Mirada", //labels the pin 
                Address = "3697 La Mirada Dr, San Marcos, CA 92078" //sets the address of the pinned location
            };
            var pin2 = new Pin()
            {
                Position = new Position(22.883170,-109.906287), // makes the position for the pin at the provided coordinates
                Label = "Cabo San Lucas", //labels the pin 
                Address = "Baja California Sur, Mexico" //sets the address of the pinned location
            };
            var pin3 = new Pin()
            {
                Position = new Position(34.927103, -120.448531), // makes the position for the pin at the provided coordinates
                Label = "Birthplace", //labels the pin 
                Address = "Santa Maria California" //sets the address of the pinned location
            };
            //adds the pin to the location specified by the coordinates on the map
            map1.Pins.Add(pin); 
            map1.Pins.Add(pin1);
            map1.Pins.Add(pin2);
            map1.Pins.Add(pin3);
        }

        private void picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var pick = sender as Picker;
            int select = pick.SelectedIndex;

            if(select != -1)
            {
                if(select == 0)
                { 
                    map1.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(34.244272, -118.442173), Distance.FromMiles(10)));
                }
                if (select == 1)
                {
                    map1.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.143447, -117.203117), Distance.FromMiles(10)));
                }
                if (select == 2)
                {
                    map1.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(34.927103, -120.448531), Distance.FromMiles(10)));
                }
                if (select == 3)
                {
                    map1.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(22.883170, -109.906287), Distance.FromMiles(10)));
                }
            }
        }
    }
}