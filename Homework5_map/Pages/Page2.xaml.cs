﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Homework5_map.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        public ObservableCollection<string> list1 { get; set; }
        public Page2()
        {
            InitializeComponent();
            location();
            pins();
        }
        //this will populate the picker menu
        void location()
        {
            //makes a list for the picker
            list1 = new ObservableCollection<string>();

            //adds the string of the title for the specific cell
            list1.Add("Mom's birthplace");
            list1.Add("Famous Museum");
            list1.Add("Awesome Vacation");
            list1.Add("Vacation spot 2");

            //adds the items to the picker
            picker.ItemsSource = list1;

        }
        void pins()
        {
            //sets the pin positon, label, and address
            var pin = new Pin()
            {
                Position = new Position(20.149430, -101.709805),// makes the position for the pin at the provided coordinates
                Label = "Mother's Birthplace", //labels the pin 
                Address = "Angamacutiro de la union, Michoacan" //sets the address of the pinned location
            };
            var pin1 = new Pin()
            {
                Position = new Position(21.020329, -101.266111), // makes the position for the pin at the provided coordinates
                Label = "Museum of Mummies", //labels the pin 
                Address = "Guanajuato" //sets the address of the pinned location
            };
            var pin2 = new Pin()
            {
                Position = new Position(20.651702, -105.243661), // makes the position for the pin at the provided coordinates
                Label = "Puerto Vallarata", //labels the pin 
                Address = "Puerto Vallarta, Jaliso, Mexico" //sets the address of the pinned location
            };
            var pin3 = new Pin()
            {
                Position = new Position(23.245917, -106.454538), // makes the position for the pin at the provided coordinates
                Label = "Golden Zone", //labels the pin 
                Address = "Mazatlan, Sinaloa, Mexico" //sets the address of the pinned location
            };
            //adds the pin to the location specified by the coordinates on the map
            map2.Pins.Add(pin);
            map2.Pins.Add(pin1);
            map2.Pins.Add(pin2);
            map2.Pins.Add(pin3);
        }
        private void picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var pick = sender as Picker;
            int choice = pick.SelectedIndex;
            if (choice != -1)
            {
                if (choice == 0)
                {
                    map2.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(20.149430, -101.709805), Distance.FromMiles(10)));
                }
                if (choice == 1)
                {
                    map2.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(21.020329, -101.266111), Distance.FromMiles(10)));
                }
                if (choice == 2)
                {
                    map2.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(20.651702, -105.243661), Distance.FromMiles(10)));
                }
                if (choice == 3)
                {
                    map2.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(23.245917, -106.454538), Distance.FromMiles(10)));
                }
            }
        }
    }
}