﻿using Homework5_map.Pages;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Homework5_map
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            var tabbedPage = new TabbedPage();
            tabbedPage.Children.Add(new Page1()); //creates a child page for tab
            tabbedPage.Children.Add(new Page2()); //creates a child page for tab
            tabbedPage.Children.Add(new Page3()); //creates a child page for tab
            MainPage = new TabbedPage(); //my main page will be the tabbed page that will have the children attached to it
         
            MainPage = tabbedPage; //makes the main page the where the tabbs will be shown
        }
        //"com.google.android.geo.API_KEY"
        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
